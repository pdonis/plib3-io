#!/usr/bin/env python3
"""
Sub-Package COMM of Package PLIB3 -- "Child" Communication
Copyright (C) 2008-2015 by Peter A. Donis

Released under the GNU General Public License, Version 2
See the LICENSE and README files for more information

This sub-package contains utilities for working with and
managing and communicating with child threads and processes.
"""
